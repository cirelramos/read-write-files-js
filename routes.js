
module.exports = function (app) {
  let fs = require('fs');
  let getStringValues = (line, str) => {
    let strTemp = str;
    let strEnd = line.length;
    let strStart = str.length + 1;
    let strNew = line.substring(strStart, strEnd);
    return strNew;
  };
  let getValuePartNetwork = (array) => {
    let arrayReturn = [];
    let negative = -1;
    if (array.length > 0) {
      array.forEach((line) => {
        let strV = 'allow-hotplug';
        if (line.search(strV) !== negative) {
          arrayReturn[strV] = getStringValues(line, strV);
        }
        strV = `iface ${arrayReturn[strV]} inet`;
        if (line.search(strV) !== negative) {
          arrayReturn[strV] = getStringValues(line, strV);
        }
        if (arrayReturn[`iface ${arrayReturn['allow-hotplug']} inet`] === 'static') {
          strV = 'address';
          if (line.search(strV) !== negative) {
            arrayReturn[strV] = getStringValues(line, strV);
          }
          strV = 'netmask';
          if (line.search(strV) !== negative) {
            arrayReturn[strV] = getStringValues(line, strV);
          }
          strV = 'gateway';
          if (line.search(strV) !== negative) {
            arrayReturn[strV] = getStringValues(line, strV);
          }
        }
      });
    }
    return arrayReturn;
  };
  let findEth = (array, eth) => {
    let search1 = '';
    let count = -1;
    let negative = -1;
    let arrayReturn = [];
    let arrayReturn2 = [];
    if (array.length > 0) {
      array.forEach((line) => {
        search1 = line.search('#');
        if (search1 !== negative) { count += 1; }
        if (count === eth) { arrayReturn.push(line); }
        // console.log(`${line} search1=  ${search1}  count=   ${count}  eth=  ${eth}`);
      });
    }
    // console.log(arrayReturn);
    arrayReturn = getValuePartNetwork(arrayReturn);
    arrayReturn2[`eth${eth}`] = arrayReturn;
    return arrayReturn2;
  };

  let findFileWithFS = (req, res) => {
    let tmpUsers = fs.readFileSync('interfaces', 'utf8');
    const aux = tmpUsers.replace(/; (.*)\n|;\t(.*)\n|;\n|;-(.*)\n/g, '');
    const lines = aux.replace(/\n\n/g, '\n').trim();
    const fields = lines.split('\n');
    let eth0 = findEth(fields, 0);
    var eth1 = findEth(fields, 1);
    let returnHtml = `<div>${fields}</div>`;
    returnHtml += '<div>Values Separate</div>';
    returnHtml += `<div>${eth0}</div>`;
    console.log(eth0);
    console.log(eth1);
    res.send(returnHtml);
  };


  //= =======================================================> write files
  let writeNet = (array) => {
    let str = '';
    let count = 1;
    str += 'auto lo\n';
    str += 'iface lo inet loopback\n';

    for (let key in array) {
      str += `# The ${count} network interface\n`;
      count += 1;
      for (let key2 in array[key]) {
        str += `${key2} ${array[key][key2]}\n`;
      }
    }
    str += '# The Management network interface\n';
    str += 'auto eth0:0\n';
    str += 'iface eth0:0 inet static\n';
    str += 'address 1.1.1.253\n';
    str += 'netmask 255.255.255.0\n';
    console.log(str);
    fs.writeFileSync('newInterface', str, 'utf8');
    return str;
  };
  let startWrite = (test) => {
    let arrayForInsert = [];
    let arrayGlobal = [];
    arrayForInsert['allow-hotplug'] = 'eth0';
    arrayForInsert[`iface ${arrayForInsert['allow-hotplug']} inet`] = 'dhcp';
    arrayGlobal[arrayForInsert['allow-hotplug']] = arrayForInsert;
    arrayForInsert = [];
    arrayForInsert['allow-hotplug'] = 'eth1';
    arrayForInsert[`iface ${arrayForInsert['allow-hotplug']} inet`] = 'static';
    arrayForInsert.address = '96.71.3.43';
    arrayForInsert.netmask = '255.255.255.248';
    arrayForInsert.gateway = '96.71.3.46';
    arrayGlobal[arrayForInsert['allow-hotplug']] = arrayForInsert;
    return writeNet(arrayGlobal);
  };
  let lookingWrite = (req, res) => {
    let responM = startWrite();
    res.send(`<p>${responM}</p>`);
  };


  // API ROUTES

  app.get('/read2', findFileWithFS);
  app.get('/write', lookingWrite);
};
